import $H from '@/common/request.js'

/**
 * 上传文件
 *
 * @param file
 */
export function uploadFileApi(filePath) {
    return $H.upload('/file/upload', {
        filePath: filePath
    })
}

/**
 * 删除文件
 *
 * @param filePath 文件完整路径
 */
// export function deleteFileApi(filePath) {
    // return request({
    //     url: '/system/file/delete',
    //     method: 'post',
    //     params: {
    //         filePath: filePath
    //     }
    // });
// }