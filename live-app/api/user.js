import $H from '@/common/request.js'

export function loginApi(data) {
	return $H.request({
		method: 'POST',
		url: '/auth/login',
		header: {
			'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
		},
		data: data
	})
}

export function logoutApi() {
	return $H.request({
		method: 'POST',
		url: '/auth/logout'
	})
}

export function getUserinfo() {
	return $H.request({
		method: 'POST',
		url: '/user/info'
	})
}

export function addUser(data) {
	return $H.request({
		method: 'POST',
		url: '/user/add',
        data: data
	})
}

export function updateInfo(data) {
	return $H.request({
		method: 'POST',
		url: '/user/update',
        data: data
	})
}

export function queryLiveUser(data) {
	return $H.request({
		method: 'POST',
		url: '/user/page',
        data: data
	})
}
