import $H from '@/common/request.js'

export function getGiftList() {
	return $H.request({
		method: 'GET',
		url: '/gift/list'
	})
}



