import $H from '@/common/request.js'

export function getLivePage(data) {
	return $H.request({
		method: 'GET',
		url: '/live/page',
		data: data
	})
}

export function getLive(id) {
	return $H.request({
		method: 'GET',
		url: `/live/${id}`
	})
}

export function createLive(data) {
	return $H.request({
		method: 'POST',
		url: '/live/create',
		data: data
	})
}

export function updateStatus(id, status) {
	return $H.request({
		method: 'POST',
		url: `/live/updateStatus/${id}`,
        header: {
        	'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
		data: {
            status: status
        }
	})
}


