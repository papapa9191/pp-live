export default {
	/*
	注意：以下接口改成你自己部署的后端（老师提供的api接口已关闭），不懂的请看：课时127-131这几节课。
	*/
	
	baseUrl:"http://192.168.1.26:8085/app",
	socketUrl:"ws://192.168.1.26:9555",
	imageUrl:"http://192.168.1.26:9000",
	
	// 拉流前缀
	livePlayBaseUrl:"http://192.168.1.26/hls",
	// 推流前缀
	livePushBaseUrl:"rtmp://192.168.1.26:1935/live",
}