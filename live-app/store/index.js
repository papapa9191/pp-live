import Vue from 'vue'
import Vuex from 'vuex'

// import $H from '../common/request.js';
import $C from '../common/config.js';
import io from '../common/uni-socket.io.js';
import { loginApi, logoutApi, getUserinfo } from '../api/user.js';

Vue.use(Vuex)

export default new Vuex.Store({
	state:{
		user:null,
		token:null,
		socket:null
	},
	actions:{
		// 断开socket连接
		closeSocket({ state,dispatch }){
			if(state.socket){
				state.socket.close()
			}
		},
		// 连接socket
		connectSocket({ state, dispatch }, roomId){
            if(state.socket){
                dispatch("closeSocket")
            }
            
			const S = io($C.socketUrl,{
				query:{
                    roomId: roomId,
                    userId: state.user.id,
                    token: state.token
                },
				transports:['websocket'],
				timeout:5000
			})
			
			// 监听连接
			S.on('connect',()=>{
				console.log('已连接')
				state.socket = S
			})
            // socket.io唯一链接id，可以监控这个id实现点对点通讯
            S.on(S.id,(e) => {
            	console.log(e)
            	return uni.showToast({
            		title: e.message,
            		icon: 'none'
            	});
            })
            // 监听用户进入房间
            S.on('online', e => {
                uni.$emit('live',{
                    type:"online",
                    data:e
                })
            })
            // 监听用户离开房间
            S.on('offline', e => {
                uni.$emit('live',{
                    type:"online",
                    data:e
                })
            })
            // 监听评论
            S.on('comment', e => {
                uni.$emit('live',{
                    type:"comment",
                    data:e
                })
            })
            // 监听礼物接收
            S.on('gift', e => {
                uni.$emit('live',{
                    type:"gift",
                    data:e
                })
            })
            
			// 监听失败
			S.on('error',()=>{
				state.socket = null
				console.log('连接失败')
			})
            
			// 监听断开
			S.on('disconnect',()=>{
				state.socket = null
				console.log('已断开')
			})
            
		},
		authMethod({ state },callback){
			if(!state.token){
				uni.showToast({
					title: '请先登录',
					icon: 'none'
				});
				return uni.navigateTo({
					url: '/pages/login/login'
				});
			}
			callback()
		},
		// 初始化用户登录状态
		initUser({ state,dispatch }){
			let u = uni.getStorageSync('user')
			let t = uni.getStorageSync('token')
			if(u){
				state.user = JSON.parse(u)
				state.token = t
				// 连接socket
				// console.log('连接socket')
				// dispatch('connectSocket')
			}
		},
        login({ state, dispatch }, formData){
            return new Promise((res, rej) => {
                loginApi(formData).then(({ data }) => {
                    state.token = data
                    uni.setStorageSync('token', data)
                    
                    // 连接socket
                    // console.log('连接socket')
                    // dispatch('connectSocket')
                    
                    res(data)
                }).catch((err) => {
                    rej(err)
                })
            })
        },
		logout({ state, dispatch }){
			return new Promise((res) => {
                logoutApi().finally(() => {
                    // console.log('断开socket')
                    // dispatch('closeSocket')
                    
                    state.user = null
                    state.token = null
                    uni.removeStorageSync('user')
                    uni.removeStorageSync('token')
                    
                    res()
                })
            })
		},
        getUserInfo({ state }){
            if(state.token == null){
                state.token = uni.getStorageSync('token')
            }
            return new Promise((res, rej) => {
                getUserinfo().then(({ data }) => {
                    state.user = data
                    uni.setStorageSync('user', JSON.stringify(data))
                    
                    res(data)
                }).catch((err) => {
                    rej(err)
                })
            })
        }
	}
})