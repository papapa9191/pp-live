import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'

// import $H from './common/request.js';
// Vue.prototype.$H = $H

import store from './store/index.js';
Vue.prototype.$store = store

Vue.prototype.authJump = (options)=>{
	if(!store.state.user){
		uni.showToast({
			title: '请先登录',
			icon: 'none'
		});
		return uni.navigateTo({
			url: '/pages/login/login'
		});
	}
	uni.navigateTo(options);
}


Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
  ...App
})
app.$mount()
// #endif
