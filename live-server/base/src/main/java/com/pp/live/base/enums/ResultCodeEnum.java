package com.pp.live.base.enums;

/**
 * 枚举了一些常用API操作码
 * Created by macro on 2019/4/19.
 */
public enum ResultCodeEnum {
	
    SUCCESS(20000, "操作成功"),
    ERROR(50000, "内部错误"),
    VALIDFAILED(50001, "参数检验失败"),
    UNAUTHORIZED(1401, "请先通过身份认证"),
    FORBIDDEN(1403, "没有相关权限"),
    NOTFOUND(1404, "路径错误");
	
    private long code;
    private String message;

    private ResultCodeEnum(long code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
