package com.pp.live.base.mapper;

import com.pp.live.base.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

}
