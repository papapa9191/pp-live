package com.pp.live.base.model;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.Data;

/**
 * 分页数据封装类
 * Created by macro on 2019/4/19.
 */
@Data
public class PageQuery<T> {
	
    private long pageNum;
	
    private long pageSize;
	
	/**
     * 封装分页对象
     * @return
     */
    public IPage<T> getPage() {
        return new Page<T>(pageNum, pageSize);
    }

}
