package com.pp.live.base.mapper;

import com.pp.live.base.entity.User;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

	IPage<User> queryPage(IPage<User> page, @Param(Constants.WRAPPER) QueryWrapper<User> wrapper);

}
