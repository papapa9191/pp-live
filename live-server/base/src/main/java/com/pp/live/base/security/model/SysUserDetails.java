package com.pp.live.base.security.model;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Spring Security
 *
 * @author haoxr
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserDetails implements UserDetails {

	private static final long serialVersionUID = 5627166853051970238L;
	
	private Integer id;

	private String password;

	private String username;

	private List<SysGrantedAuthority> authorities;

	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled;
	
	private String avatar;
	
	private Integer coin;
}
