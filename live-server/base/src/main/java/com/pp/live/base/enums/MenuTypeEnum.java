package com.pp.live.base.enums;

public enum MenuTypeEnum {

	CATALOG(1, "目录"),
	MENU(2, "菜单"),
	BUTTON(3, "菜单"),
	EXTLINK(4, "外链")	;
	
	private Integer code;
	private String name;
	
	private MenuTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
