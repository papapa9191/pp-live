package com.pp.live.base.oss.service;

import org.springframework.web.multipart.MultipartFile;

public interface IOssService {

	/**
     * 上传文件
     * @param file 表单文件对象
     * @return 文件信息
	 * @throws Exception 
     */
    String uploadFile(MultipartFile file) throws Exception;

    /**
     * 删除文件
     *
     * @param filePath 文件完整URL
     * @return 删除结果
     * @throws Exception 
     */
    boolean deleteFile(String filePath) throws Exception;
}
