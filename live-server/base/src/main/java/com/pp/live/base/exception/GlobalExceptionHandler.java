package com.pp.live.base.exception;

import java.sql.SQLSyntaxErrorException;

import org.springframework.http.HttpStatus;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.pp.live.base.model.Result;

import lombok.extern.slf4j.Slf4j;

/**
 * 全局系统异常处理器
 * <p>
 * 调整异常处理的HTTP状态码，丰富异常处理类型
 *
 * @author Gadfly
 * @since 2020-02-25 13:54
 **/
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	
	@ResponseBody
	@ExceptionHandler(ValidateException.class)
	public Result<?> handleBizException(ValidateException e) {
		log.error("biz exception: {}", e.getMessage());
		return Result.validateFailed(e.getMessage());
	}
	
	@ResponseBody
    @ExceptionHandler(BadCredentialsException.class)
    public Result<?> processBadCredentialsException(BadCredentialsException e) {
        log.error(e.getMessage(), e);
        return Result.failed(e.getMessage());
    }
	
	@ResponseBody
	@ExceptionHandler(BadSqlGrammarException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Result<?> handleBadSqlGrammarException(BadSqlGrammarException e) {
        log.error(e.getMessage(), e);
        return Result.failed(e.getMessage());
    }

	@ResponseBody
    @ExceptionHandler(SQLSyntaxErrorException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Result<?> processSQLSyntaxErrorException(SQLSyntaxErrorException e) {
        log.error(e.getMessage(), e);
        return Result.failed(e.getMessage());
    }
	
}
