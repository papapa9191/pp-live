package com.pp.live.base.config;

import org.springdoc.core.customizers.GlobalOpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;

import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

/**
 * Swagger基础配置
 * Swagger首页：http://ip:port/swagger-ui/index.html
 */
public abstract class BaseSwaggerConfig {
	
	/**
     * 接口信息
     */
    @Bean
    public OpenAPI openApi() {
        return new OpenAPI()
                .info(apiInfo())
                // 全局安全校验项，也可以在对应的controller上加注解SecurityRequirement
                .components(new Components()
                        .addSecuritySchemes(HttpHeaders.AUTHORIZATION,
                                new SecurityScheme()
                                        .name(HttpHeaders.AUTHORIZATION)
                                        .type(SecurityScheme.Type.APIKEY)
                                        .in(SecurityScheme.In.HEADER)
//                                        .scheme("Bearer")
//                                        .bearerFormat("JWT")
                        )
                )
                .addSecurityItem(new SecurityRequirement().addList(HttpHeaders.AUTHORIZATION));
    }


    @Bean
    public GlobalOpenApiCustomizer globalOpenApiCustomizer() {
        return openApi -> openApi.getPaths().values()
                .stream()
                .flatMap(pathItem -> pathItem.readOperations().stream())
                .forEach(operation -> operation.security(openApi.getSecurity()));
    }
	
    protected abstract Info apiInfo();

}
