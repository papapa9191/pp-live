package com.pp.live.base.oss.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.pp.live.base.oss.service.IOssService;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.ObjectWriteArgs;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.SetBucketPolicyArgs;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

/**
 * MinIO 文件上传服务类
 *
 * @author haoxr
 * @since 2023/6/2
 */
@Component
@ConditionalOnProperty(value = "oss.type", havingValue = "minio")
@ConfigurationProperties(prefix = "oss.minio")
@RequiredArgsConstructor
@Data
public class MinioOssService implements IOssService {

    /**
     * 服务Endpoint(http://localhost:9000)
     */
    private String endpoint;
    /**
     * 访问凭据
     */
    private String accessKey;
    /**
     * 凭据密钥
     */
    private String secretKey;
    /**
     * 存储桶名称
     */
    private String bucketName;

    private MinioClient minioClient;

    // 依赖注入完成之后执行初始化
    @PostConstruct
    public void init() {
    	//创建一个MinIO的Java客户端
        minioClient = MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, secretKey)
                .build();
        //创建存储桶并设置只读权限
        createBucketIfAbsent(bucketName);
    }


    /**
     * 上传文件
     *
     * @param file 表单文件对象
     * @return
     * @throws Exception
     */
    @Override
    public String uploadFile(MultipartFile file) throws Exception {
    	
    	String filename = file.getOriginalFilename();
        // 设置存储对象名称
        String objectName = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "/" 
        	+ UUID.randomUUID().toString().replace("-", "") + filename.substring(filename.lastIndexOf("."));
        // 使用putObject上传一个文件到存储桶中
        PutObjectArgs putObjectArgs = PutObjectArgs.builder()
		        .bucket(bucketName)
		        .object(objectName)
		        .contentType(file.getContentType())
		        .stream(file.getInputStream(), file.getSize(), ObjectWriteArgs.MIN_MULTIPART_SIZE)
		        .build();
        minioClient.putObject(putObjectArgs);
//        file.getInputStream().close();
        
        return String.format("/%s/%s", bucketName, objectName);
    }


    /**
     * 删除文件
     *
     * @param filePath 文件路径https://oss.youlai.tech/default/20221120/test.jpg
     * @return
     * @throws Exception
     */
    @Override
    public boolean deleteFile(String filePath) throws Exception {
    	
        String objectName = filePath.replaceAll("^" + endpoint + "/" + bucketName + "/", "");
		minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
		
		return true;
    }


    /**
     * PUBLIC桶策略
     * 如果不配置，则新建的存储桶默认是PRIVATE，则存储桶文件会拒绝访问 Access Denied
     *
     * @param bucketName
     * @return
     */
    private static String publicBucketPolicy(String bucketName) {
        /**
         * AWS的S3存储桶策略
         * Principal: 生效用户对象
         * Resource:  指定存储桶
         * Action: 操作行为, 只读
         */
    	return "{\"Version\":\"2012-10-17\","
        + "\"Statement\":[{\"Effect\":\"Allow\","
        + "\"Principal\":{\"AWS\":[\"*\"]},"
        + "\"Action\":[\"s3:ListBucketMultipartUploads\",\"s3:GetBucketLocation\",\"s3:ListBucket\"],"
        + "\"Resource\":[\"arn:aws:s3:::" + bucketName + "\"]},"
        + "{\"Effect\":\"Allow\"," + "\"Principal\":{\"AWS\":[\"*\"]},"
        + "\"Action\":[\"s3:ListMultipartUploadParts\",\"s3:PutObject\",\"s3:AbortMultipartUpload\",\"s3:DeleteObject\",\"s3:GetObject\"],"
        + "\"Resource\":[\"arn:aws:s3:::" + bucketName + "/*\"]}]}";
    }

    /**
     * 创建存储桶(存储桶不存在)
     *
     * @param bucketName
     */
    @SneakyThrows
    private void createBucketIfAbsent(String bucketName) {
        BucketExistsArgs bucketExistsArgs = BucketExistsArgs.builder().bucket(bucketName).build();
        if (!minioClient.bucketExists(bucketExistsArgs)) {
            MakeBucketArgs makeBucketArgs = MakeBucketArgs.builder().bucket(bucketName).build();

            minioClient.makeBucket(makeBucketArgs);

            // 设置存储桶访问权限为PUBLIC， 如果不配置，则新建的存储桶默认是PRIVATE，则存储桶文件会拒绝访问 Access Denied
            SetBucketPolicyArgs setBucketPolicyArgs = SetBucketPolicyArgs
                    .builder()
                    .bucket(bucketName)
                    .config(publicBucketPolicy(bucketName))
                    .build();
            minioClient.setBucketPolicy(setBucketPolicyArgs);
        }
    }
}
