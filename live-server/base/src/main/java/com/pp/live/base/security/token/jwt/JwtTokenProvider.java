package com.pp.live.base.security.token.jwt;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.pp.live.base.security.constant.SecurityConstants;
import com.pp.live.base.security.model.SysUserDetails;
import com.pp.live.base.security.token.TokenProvider;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;

/**
 * JWT token 管理器
 *
 * @author haoxr
 * @since 2023/9/13
 */

@Component
public class JwtTokenProvider implements TokenProvider {

	@Value("${jwt.secret-key:d76bd789-40bb-40cb-a651-5ba4ebcd8889}")
	private String secretKey;

	@Value("${jwt.expire:1440}")
	private int expire;
	
	@Autowired
    private RedisTemplate<String, Object> redisTemplate;
	
	@Override
	public String createToken(Authentication authentication) {
		Claims claims = Jwts.claims().setSubject(authentication.getName());

		SysUserDetails userDetails = (SysUserDetails) authentication.getPrincipal();
		claims.put("userDetails", JSON.toJSONString(userDetails));
		claims.put(SecurityConstants.JWT_ID, UUID.randomUUID().toString());
		
		Date now = new Date();
		// JWT默认5天有效期
		Date expirationTime = new Date(now.getTime() + 1440 * 60 * 1000L);
		
		String token = Jwts.builder().setClaims(claims).setIssuedAt(now)
				.setExpiration(expirationTime)
				.signWith(Keys.hmacShaKeyFor(getSecretKeyBytes()), SignatureAlgorithm.HS256)
				.compact();

		redisTemplate.opsForValue().set(SecurityConstants.JWT_ID_CACHE_PREFIX + claims.get("jti", String.class), userDetails.getUsername(), expire, TimeUnit.MINUTES);
		return SecurityConstants.TOKEN_PREFIX + token;
	}

	@Override
	public Authentication getAuthentication(String token) {
		Claims claims = this.getTokenClaims(token);
		SysUserDetails userDetails = JSON.parseObject(claims.get("userDetails", String.class), SysUserDetails.class);
//		redisTemplate.expire("jti:" + claims.get("jti", String.class), expire, TimeUnit.MINUTES);
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}

	@Override
	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader(SecurityConstants.TOKEN_KEY);
		if (bearerToken != null && bearerToken.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			return bearerToken;
		}
		return null;
	}

	@Override
	public boolean validateToken(String token) {
		
		if(token != null && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			Claims claims = getTokenClaims(token);
			if(claims != null && validateJti(claims.get(SecurityConstants.JWT_ID, String.class))) {
				return true;
			}
		}
				
		return false;
	}
	
	@Override
	public void removeToken(String token) {
		Claims claims = getTokenClaims(token);
		if(claims != null) {
			redisTemplate.delete(SecurityConstants.JWT_ID_CACHE_PREFIX + claims.get(SecurityConstants.JWT_ID, String.class));
		}
	}

	@Override
	public Claims getTokenClaims(String token) {
		Claims claims = null;
		// 会抛异常
		claims = Jwts.parserBuilder().setSigningKey(this.getSecretKeyBytes()).build()
				.parseClaimsJws(token.substring(7)).getBody();
		return claims;
	}
	
	@Override
	public boolean validateJti(String jti) {
		
		if(jti != null && redisTemplate.getExpire(SecurityConstants.JWT_ID_CACHE_PREFIX + jti, TimeUnit.MINUTES) > 0) {
			return true;
		}
				
		return false;
	}

	private byte[] getSecretKeyBytes() {
		return secretKey.getBytes(StandardCharsets.UTF_8);
	}

}
