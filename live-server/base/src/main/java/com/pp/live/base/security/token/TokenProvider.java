package com.pp.live.base.security.token;

import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;

public interface TokenProvider {

	/**
	 * 创建Token
	 *
	 * @param authentication
	 * @return
	 */
	String createToken(Authentication authentication);
	
	/**
	 * 获取认证信息
	 *
	 * @param token
	 * @return
	 */
	Authentication getAuthentication(String token);
	
	/**
	 * 获取token
	 *
	 * @param req
	 * @return
	 */
	String resolveToken(HttpServletRequest req);
	
	/**
	 * 验证token是否有效
	 *
	 * @param token
	 * @return
	 */
	boolean validateToken(String token);
	
	/**
	 * 删除token
	 *
	 * @param token
	 * @return
	 */
	void removeToken(String token);

	Claims getTokenClaims(String token);

	boolean validateJti(String jti);
}
