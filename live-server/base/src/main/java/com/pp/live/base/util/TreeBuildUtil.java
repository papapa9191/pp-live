package com.pp.live.base.util;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class TreeBuildUtil {

	/**
	 * @param <T> 必须含有字段: 主键->id, 上级主键->parentId, 下级列表->children
	 * @param list
	 * @param root
	 * @return
	 * @throws Exception
	 */
	public static <T> void treeBuild(List<T> list, T root) throws Exception{
		if(root == null || list == null || list.size() == 0) {
			return;
		}
		Class<? extends Object> clazz = root.getClass();
		Field pidField = clazz.getDeclaredField("parentId");
		pidField.setAccessible(true);
		Field idField = clazz.getDeclaredField("id");
		idField.setAccessible(true);
		List<T> childList = list.stream().filter(e -> {
			try {
				return idField.get(root).equals(pidField.get(e));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
//				e1.printStackTrace();
				return false;
			}
		}).collect(Collectors.toList());
		
		for (T t : childList) {
			treeBuild(list, t);
		}
		
		Field childField = clazz.getDeclaredField("children");
		childField.setAccessible(true);
		childField.set(root, childList);
	}
		
}
