package com.pp.live.base.security.model;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SysGrantedAuthority implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5867732227002446594L;
	private String authority;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof GrantedAuthority) {
			return this.authority.equals(((GrantedAuthority) obj).getAuthority());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.authority.hashCode();
	}
}
