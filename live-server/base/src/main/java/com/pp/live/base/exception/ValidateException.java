package com.pp.live.base.exception;

public class ValidateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1107902714810503754L;

	public ValidateException(String message){
        super(message);
    }

	public ValidateException(String message, Throwable cause){
        super(message, cause);
    }

	public ValidateException(Throwable cause){
        super(cause);
    }
}
