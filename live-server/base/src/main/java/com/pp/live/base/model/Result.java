package com.pp.live.base.model;

import com.pp.live.base.enums.ResultCodeEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用返回对象
 * Created by macro on 2019/4/19.
 */
@Data
@NoArgsConstructor // 无参构造方法
@AllArgsConstructor // 有参构造方法
public class Result <T> {
    private long code;
    private String message;
    private T data;
    
    public Result(ResultCodeEnum resultCode) {
    	this.code = resultCode.getCode();
    	this.message = resultCode.getMessage();
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Result<T> success() {
        return new Result<T>(ResultCodeEnum.SUCCESS);
    }
    
    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Result<T> success(T data) {
        return new Result<T>(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 失败返回结果
     */
    public static <T> Result<T> failed() {
    	return new Result<T>(ResultCodeEnum.ERROR);
    }
    
    /**
     * 失败返回结果
     * @param message 提示信息
     */
    public static <T> Result<T> failed(String message) {
    	return new Result<T>(ResultCodeEnum.ERROR.getCode(), message, null);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> Result<T> validateFailed() {
        return new Result<T>(ResultCodeEnum.VALIDFAILED);
    }

    /**
     * 参数验证失败返回结果
     * @param message 提示信息
     */
    public static <T> Result<T> validateFailed(String message) {
        return new Result<T>(ResultCodeEnum.VALIDFAILED.getCode(), message, null);
    }

    /**
     * 未登录返回结果
     */
    public static <T> Result<T> unauthorized() {
        return new Result<T>(ResultCodeEnum.UNAUTHORIZED);
    }

    /**
     * 未授权返回结果
     */
    public static <T> Result<T> forbidden() {
        return new Result<T>(ResultCodeEnum.FORBIDDEN);
    }
    
    /**
     * 找不到页面
     */
    public static <T> Result<T> notFound() {
        return new Result<T>(ResultCodeEnum.NOTFOUND);
    }
}
