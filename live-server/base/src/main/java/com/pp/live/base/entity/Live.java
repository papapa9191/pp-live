package com.pp.live.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Getter
@Setter
@TableName("live")
@Schema(name = "Live", description = "")
public class Live implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @Schema(description = "直播间标题")
    @TableField("title")
    private String title;

    @Schema(description = "直播间封面")
    @TableField("cover")
    private String cover;

    @Schema(description = "用户id")
    @TableField("user_id")
    private Integer userId;

    @Schema(description = "总观看人数")
    @TableField("look_count")
    private Integer lookCount;

    @Schema(description = "总金币")
    @TableField("coin")
    private Integer coin;

    @Schema(description = "唯一标识")
    @TableField("`key`")
    private String key;

    @Schema(description = "直播间状态 0未开播 1直播中 2暂停直播 3直播结束")
    @TableField("status")
    private Integer status;

    @TableField("created_time")
    private LocalDateTime createdTime;

    @TableField("updated_time")
    private LocalDateTime updatedTime;
    
    @TableField(exist = false)
    private User user;
    
    @Schema(description = "推流校验码")
    @TableField(exist = false)
    private String sign;
}
