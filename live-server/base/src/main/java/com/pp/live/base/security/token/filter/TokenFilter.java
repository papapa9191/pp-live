package com.pp.live.base.security.token.filter;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.pp.live.base.security.token.TokenProvider;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 
 * @since 2023/9/13
 */
@Component
@Slf4j
public class TokenFilter extends OncePerRequestFilter {

	@Autowired
	private TokenProvider tokenProvider;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String token = tokenProvider.resolveToken(request);

		if (token != null) {
			try {
				if(tokenProvider.validateToken(token)) {
					Authentication auth = tokenProvider.getAuthentication(token);
					SecurityContextHolder.getContext().setAuthentication(auth);
				}
	        } catch (Exception e) {
	        	log.error("token校验失败: {}", e.getMessage());
	        	// 校验token失败
	            SecurityContextHolder.clearContext();
	        }
		}

		filterChain.doFilter(request, response);
		
	}
}
