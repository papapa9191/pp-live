package com.pp.live.app.service.impl;

import com.pp.live.app.service.ILiveService;
import com.pp.live.app.service.ILiveUserService;
import com.pp.live.base.entity.Live;
import com.pp.live.base.entity.LiveUser;
import com.pp.live.base.mapper.LiveUserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class LiveUserServiceImpl extends ServiceImpl<LiveUserMapper, LiveUser> implements ILiveUserService {
	
	@Autowired
	private ILiveService liveService;

	@Transactional
	@Override
	public boolean save(LiveUser entity) {
		QueryWrapper<LiveUser> wrapper = new QueryWrapper<>();
		wrapper.eq("live_id", entity.getLiveId());
		wrapper.eq("user_id", entity.getUserId());
		wrapper.last("limit 1");
		LiveUser liveUser = baseMapper.selectOne(wrapper);
		if(liveUser == null) {
			Live live = liveService.getById(entity.getLiveId());
			live.setLookCount(live.getLookCount() + 1);
			live.setUpdatedTime(LocalDateTime.now());
			liveService.updateById(live);
			
			entity.setCreatedTime(LocalDateTime.now());
			return super.save(entity);
		}else {
			entity.setId(liveUser.getId());
			entity.setUpdatedTime(LocalDateTime.now());
			return super.updateById(entity);
		}
	}

}
