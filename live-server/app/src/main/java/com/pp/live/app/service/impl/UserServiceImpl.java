package com.pp.live.app.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pp.live.app.dto.UserQueryParam;
import com.pp.live.app.service.IUserService;
import com.pp.live.base.entity.User;
import com.pp.live.base.exception.ValidateException;
import com.pp.live.base.mapper.UserMapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User getByUsername(String username) {
		QueryWrapper<User> wrapper = new QueryWrapper<>();
		wrapper.eq("username", username);
		return baseMapper.selectOne(wrapper);
	}

	@Override
	public int addUser(User entity) throws ValidateException {
		entity.setCreatedTime(LocalDateTime.now());
		if(entity.getPassword() != null && !"".equals(entity.getPassword())) {
			entity.setPassword(passwordEncoder.encode(entity.getPassword()));
		}
		
		// 校验用户名
		QueryWrapper<User> wrapper = new QueryWrapper<>();
		wrapper.eq("username", entity.getUsername());
		wrapper.last("limit 1");
		if(baseMapper.selectOne(wrapper) != null)
			throw new ValidateException("用户名已经被使用！");
		
		return baseMapper.insert(entity);
	}

	
	@Override
	public IPage<User> queryPage(UserQueryParam params) {
		QueryWrapper<User> wrapper = new QueryWrapper<>();
		if(params.getLiveId() != null) {
			wrapper.eq("live_user.id", params.getLiveId());
			if(params.getStatus() != null) {
				wrapper.eq("live_user.status", params.getStatus());
			}
			wrapper.orderByDesc("live_user.updated_time");
			return baseMapper.queryPage(params.getPage(), wrapper);
		}else {
			
			return baseMapper.selectPage(params.getPage(), wrapper);
		}
	}

	@Override
	public int update(User entity) {
		if(entity.getPassword() != null && !"".equals(entity.getPassword())) {
			entity.setPassword(passwordEncoder.encode(entity.getPassword()));
		}
		entity.setUpdatedTime(LocalDateTime.now());
		return baseMapper.updateById(entity);
	}

}
