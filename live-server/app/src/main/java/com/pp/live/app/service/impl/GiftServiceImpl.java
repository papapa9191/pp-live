package com.pp.live.app.service.impl;

import com.pp.live.app.service.IGiftService;
import com.pp.live.base.entity.Gift;
import com.pp.live.base.mapper.GiftMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class GiftServiceImpl extends ServiceImpl<GiftMapper, Gift> implements IGiftService {

}
