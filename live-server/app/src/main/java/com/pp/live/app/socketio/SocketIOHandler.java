package com.pp.live.app.socketio;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.pp.live.app.service.ICommentService;
import com.pp.live.app.service.ILiveGiftService;
import com.pp.live.app.service.ILiveUserService;
import com.pp.live.base.entity.Comment;
import com.pp.live.base.entity.LiveGift;
import com.pp.live.base.entity.LiveUser;
import com.pp.live.base.model.Result;
import com.pp.live.base.security.model.SysUserDetails;
import com.pp.live.base.security.token.TokenProvider;

import io.jsonwebtoken.Claims;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SocketIOHandler {
 
    @Autowired
    private SocketIOServer socketIoServer;
    
    @Autowired
    private ILiveUserService liveUserService;
    
    @Autowired
    private ILiveGiftService liveGiftService;
    
    @Autowired
    private ICommentService commentService;
    
    @Autowired
    private TokenProvider tokenProvider;
 
    @PostConstruct
    private void autoStartup() {
        try {
            socketIoServer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("SocketIOServer启动失败");
        }
    }
 
    @PreDestroy
    private void autoStop() {
        socketIoServer.stop();
    }
 
    @OnConnect
    public void onConnect(SocketIOClient client) {
        String roomId = client.getHandshakeData().getSingleUrlParam("roomId");
        String token = client.getHandshakeData().getSingleUrlParam("token");
        
        if(!tokenProvider.validateToken(token)) {
        	client.sendEvent(client.getSessionId().toString(), Result.failed("token校验失败"));
        	client.disconnect();
        	return;
        }
        
        Claims claims = tokenProvider.getTokenClaims(token);
        SysUserDetails sysUser = JSON.parseObject(claims.get("userDetails", String.class), SysUserDetails.class);
        sysUser.setPassword(null);
        client.set("sysUser", sysUser);
        
        log.info("客户端:{}, 用户:{}, 进入房间:{}", client.getRemoteAddress(), sysUser.getUsername(), roomId);
        
        LiveUser liveUser = new LiveUser();
        liveUser.setLiveId(Integer.valueOf(roomId));
        liveUser.setUserId(sysUser.getId());
        liveUser.setStatus(1);
        liveUserService.save(liveUser);
        
        client.joinRoom(roomId);
        socketIoServer.getRoomOperations(roomId).sendEvent("online", sysUser);
    }
 
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
    	String roomId = client.getHandshakeData().getSingleUrlParam("roomId");
//    	String userId = client.getHandshakeData().getSingleUrlParam("userId");
    	SysUserDetails sysUser = client.get("sysUser");
    	
    	
    	log.info("客户端:{}, 用户:{}, 离开房间:{}", client.getRemoteAddress(), sysUser.getUsername(), roomId);
        
        LiveUser liveUser = new LiveUser();
        liveUser.setLiveId(Integer.valueOf(roomId));
        liveUser.setUserId(sysUser.getId());
        liveUser.setStatus(0);
        liveUserService.save(liveUser);
        
    	socketIoServer.getRoomOperations(roomId).sendEvent("offline", client, sysUser);
        
        client.leaveRoom(roomId);
        client.disconnect();
    }
 
    @OnEvent(value = "comment")
    public void onCommentEvent(SocketIOClient client, AckRequest ackRequest, Comment entity) {
    	String roomId = client.getHandshakeData().getSingleUrlParam("roomId");
    	SysUserDetails sysUser = client.get("sysUser");
    	entity.setUserId(sysUser.getId());
        
        log.info("客户端: {}, 发送消息:{}, 用户:{}, 房间:{}, {}", client.getRemoteAddress(), entity.getContent(), entity.getUserId(), roomId);
        
        entity.setCreatedTime(LocalDateTime.now());
        commentService.save(entity);
        
        socketIoServer.getRoomOperations(roomId).sendEvent("comment", entity);
        
        ackRequest.sendAckData(true);
    }
    
    @OnEvent(value = "gift")
    public void onGiftEvent(SocketIOClient client, AckRequest ackRequest, LiveGift entity) {
    	String roomId = client.getHandshakeData().getSingleUrlParam("roomId");
    	SysUserDetails sysUser = client.get("sysUser");
    	entity.setUserId(Integer.valueOf(sysUser.getId()));
        
    	log.info("客户端: {}, 发送礼物:{}, 用户:{}, 房间:{}, {}", client.getRemoteAddress(), entity.getGiftId(), entity.getUserId(), roomId);
        
        entity.setCreatedTime(LocalDateTime.now());
        liveGiftService.save(entity);
        
        socketIoServer.getRoomOperations(roomId).sendEvent("gift", entity);
        
        ackRequest.sendAckData(true);
    }
 
}