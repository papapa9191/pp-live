package com.pp.live.app.dto;

import com.pp.live.base.entity.User;
import com.pp.live.base.model.PageQuery;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分页数据封装类
 * Created by macro on 2019/4/19.
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class UserQueryParam extends PageQuery<User> {
	
    private Integer liveId;
    
    private Integer status;

}
