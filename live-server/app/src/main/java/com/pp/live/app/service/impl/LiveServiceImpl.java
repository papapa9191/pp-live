package com.pp.live.app.service.impl;

import com.pp.live.app.service.ILiveService;
import com.pp.live.app.service.IUserService;
import com.pp.live.base.entity.Live;
import com.pp.live.base.entity.User;
import com.pp.live.base.mapper.LiveMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class LiveServiceImpl extends ServiceImpl<LiveMapper, Live> implements ILiveService {
	
	@Autowired
	private IUserService userService;

	@Override
	public Live getById(Serializable id) {
		Live entity = super.getById(id);
		User user = userService.getById(entity.getUserId());
		user.setPassword(null);
		entity.setUser(user);
		return entity;
	}
	
	

}
