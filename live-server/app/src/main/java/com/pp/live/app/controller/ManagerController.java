package com.pp.live.app.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@RestController
@RequestMapping("/base/manager")
public class ManagerController {

}
