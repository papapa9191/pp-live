package com.pp.live.app.controller;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pp.live.app.dto.LiveAddParam;
import com.pp.live.app.dto.LiveQueryParam;
import com.pp.live.app.service.ILiveService;
import com.pp.live.base.entity.Live;
import com.pp.live.base.model.Result;
import com.pp.live.base.security.constant.SecurityConstants;
import com.pp.live.base.security.model.SysUserDetails;
import com.pp.live.base.security.token.TokenProvider;

import io.jsonwebtoken.Claims;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Tag(name = "直播")
@RestController
@RequestMapping("/live")
public class LiveController {

	@Autowired
	private ILiveService liveService;
	
	@Autowired
	private TokenProvider tokenProvider;
	
	@Operation(summary = "直播列表")
	@GetMapping("/page")
	public Result<IPage<Live>> queryPage(LiveQueryParam pageQuery){
		QueryWrapper<Live> wrapper = new QueryWrapper<>();
		if(pageQuery.getUserId() != null) {
			wrapper.eq("user_id", pageQuery.getUserId());
		}
//		wrapper.in("status", 0, 1, 2);
		wrapper.orderByDesc("created_time");
		return Result.success(liveService.page(pageQuery.getPage(), wrapper));
	}
	
	@Operation(summary = "直播")
	@GetMapping("/{id}")
	public Result<Live> query(@PathVariable Integer id){
		
		Live entity = liveService.getById(id);
		return Result.success(entity);
	}
	
	@Operation(summary = "创建直播")
	@PostMapping("/create")
	public Result<Live> create(@RequestBody LiveAddParam live, HttpServletRequest req){
		
		SysUserDetails sysUser = (SysUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		Live entity = null;
		if(live.getId() == null) {
			entity = new Live();
			entity.setCover(live.getCover());
			entity.setTitle(live.getTitle());
			entity.setKey(UUID.randomUUID().toString().replace("-", ""));
			entity.setUserId(sysUser.getId());
			entity.setCreatedTime(LocalDateTime.now());
			liveService.save(entity);
		}else {
			entity = liveService.getById(live.getId());
			entity.setCover(live.getCover());
			entity.setTitle(live.getTitle());
			entity.setUpdatedTime(LocalDateTime.now());
			liveService.updateById(entity);
		}
		
		Claims claims = tokenProvider.getTokenClaims(tokenProvider.resolveToken(req));
		entity.setSign(claims.get(SecurityConstants.JWT_ID, String.class));
		
		return Result.success(entity);
	}
	
	@Operation(summary = "状态变更")
	@PostMapping("/updateStatus/{id}")
	public Result<?> updateStatus(@PathVariable Integer id, Integer status){
		
		Live entity = new Live();
		entity.setId(id);
		entity.setStatus(status);
		entity.setUpdatedTime(LocalDateTime.now());
		liveService.updateById(entity);
		
		return Result.success();
	}
}
