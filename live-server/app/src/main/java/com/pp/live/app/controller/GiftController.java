package com.pp.live.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pp.live.app.service.IGiftService;
import com.pp.live.base.entity.Gift;
import com.pp.live.base.model.Result;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Tag(name = "直播")
@RestController
@RequestMapping("/gift")
public class GiftController {
	
	@Autowired
	private IGiftService giftService;

	@Operation(summary = "礼物列表")
	@GetMapping("/list")
	public Result<List<Gift>> queryList(){
		QueryWrapper<Gift> wrapper = new QueryWrapper<>();
		wrapper.orderByAsc("id");
		return Result.success(giftService.list(wrapper));
	}
}
