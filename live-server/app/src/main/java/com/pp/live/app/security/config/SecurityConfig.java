package com.pp.live.app.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.pp.live.base.security.constant.SecurityConstants;
import com.pp.live.base.security.exception.MyAccessDeniedHandler;
import com.pp.live.base.security.exception.MyAuthenticationEntryPoint;
import com.pp.live.base.security.token.filter.TokenFilter;

/**
 * Spring Security 权限配置
 *
 * @author haoxr
 * @since 2023/2/17
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {

	@Autowired
	private MyAuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	private MyAccessDeniedHandler accessDeniedHandler;

	@Autowired
	private TokenFilter tokenFilter;

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.authorizeHttpRequests(
					requestMatcherRegistry -> requestMatcherRegistry
						.requestMatchers(SecurityConstants.LOGIN_PATH).permitAll().anyRequest().authenticated()
				)
				.sessionManagement(
					configurer -> configurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				)
				.exceptionHandling(
					httpSecurityExceptionHandlingConfigurer -> httpSecurityExceptionHandlingConfigurer
						.authenticationEntryPoint(authenticationEntryPoint)
						.accessDeniedHandler(accessDeniedHandler)
				)
				.csrf(AbstractHttpConfigurer::disable);

		// 验证码校验过滤器
		// http.addFilterBefore(new VerifyCodeFilter(), UsernamePasswordAuthenticationFilter.class);
		// JWT 校验过滤器
		http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);
		return http.build();
	}

	/**
	 * 不走过滤器链的放行配置
	 */
	@Bean
	public WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().requestMatchers(SecurityConstants.LOGIN_PATH, "/auth/check", 
				"/webjars/**", "/doc.html", "/swagger-resources/**", "/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html", 
				"/ws/**", "/ws-app/**", "/api/**", "/user/add");
	}

	/**
	 * 密码编码器
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * AuthenticationManager 手动注入
	 * @param authenticationConfiguration 认证配置
	 */
	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
			throws Exception {
		return authenticationConfiguration.getAuthenticationManager();
	}

}
