package com.pp.live.app.service;

import com.pp.live.base.entity.Live;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
public interface ILiveService extends IService<Live> {

}
