package com.pp.live.app.service;

import com.pp.live.app.dto.UserQueryParam;
import com.pp.live.base.entity.User;
import com.pp.live.base.exception.ValidateException;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
public interface IUserService extends IService<User> {

	User getByUsername(String username);

	int addUser(User entity) throws ValidateException;

	IPage<User> queryPage(UserQueryParam params);

	int update(User entity);

}
