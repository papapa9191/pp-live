package com.pp.live.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.pp.live.base.model.Result;
import com.pp.live.base.security.token.TokenProvider;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;

@Tag(name = "认证中心")
@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenProvider tokenProvider;

	@Operation(summary = "登录系统")
	@PostMapping("/login")
	public Result<String> login(String username, String password) {

		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
				password);
		Authentication authentication = authenticationManager.authenticate(authenticationToken);

		String accessToken = tokenProvider.createToken(authentication);
		return Result.success(accessToken);
	}

	@Operation(summary = "退出登录")
	@PostMapping("/logout")
	public Result<Object> logout(HttpServletRequest req) {
		String token = tokenProvider.resolveToken(req);
		if (token != null && !"".equals(token)) {
			tokenProvider.removeToken(token);
		}
		return Result.success();
	}

	@Operation(summary = "校验")
	@PostMapping("/check")
	public Result<String> check(String sign, HttpServletRequest req) {
		System.out.println("签名: " + sign);
		
		if(tokenProvider.validateJti(sign)) {
			System.out.println("验签成功");
			return Result.success();
		}else {
			System.out.println("验签失败");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "校验失败");
		}
	}

}
