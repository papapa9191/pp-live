package com.pp.live.app.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import lombok.Data;

@Data
public class UserAddParam {

	@Schema(description = "用户名", requiredMode = RequiredMode.REQUIRED)
    private String username;

    @Schema(description = "密码", requiredMode = RequiredMode.REQUIRED)
    private String password;
    
    @Schema(description = "重复密码", requiredMode = RequiredMode.REQUIRED)
    private String rePassword;
    
    @Schema(description = "头像")
    private String avatar;
}
