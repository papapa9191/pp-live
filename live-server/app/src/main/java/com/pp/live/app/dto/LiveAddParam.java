package com.pp.live.app.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.RequiredMode;
import lombok.Data;

@Data
public class LiveAddParam {
	
	@Schema(description = "直播ID")
    private Integer id;

	@Schema(description = "直播标题", requiredMode = RequiredMode.REQUIRED)
    private String title;

    @Schema(description = "封面", requiredMode = RequiredMode.REQUIRED)
    private String cover;
}
