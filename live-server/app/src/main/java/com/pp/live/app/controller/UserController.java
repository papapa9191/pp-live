package com.pp.live.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pp.live.app.dto.UserAddParam;
import com.pp.live.app.dto.UserQueryParam;
import com.pp.live.app.service.IUserService;
import com.pp.live.base.entity.User;
import com.pp.live.base.exception.ValidateException;
import com.pp.live.base.model.Result;
import com.pp.live.base.security.model.SysUserDetails;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Tag(name = "用户中心")
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private IUserService userService;
	
	@Operation(summary = "登录信息")
	@PostMapping("/info")
	public Result<User> info() {
		
		SysUserDetails sysUser = (SysUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User entity = userService.getById(sysUser.getId());
		entity.setPassword(null);
//		User entity = new User();
//		entity.setId(sysUser.getId());
//		entity.setUsername(sysUser.getUsername());
//		entity.setAvatar(sysUser.getAvatar());
//		entity.setCoin(sysUser.getCoin());
        return Result.success(entity);
	}
	
	@Operation(summary = "用户注册")
	@PostMapping("/add")
	public Result<?> addUser(@RequestBody UserAddParam user) throws ValidateException{
		
		if(!user.getPassword().equals(user.getRePassword())) {
			return Result.validateFailed("重复输入的密码不一致！");
		}
		
		User entity = new User();
		entity.setUsername(user.getUsername());
		entity .setPassword(user.getPassword());
		return Result.success(userService.addUser(entity));
	}
	
	@Operation(summary = "修改用户信息")
	@PostMapping("/update")
	public Result<?> update(@RequestBody UserAddParam user) throws ValidateException{
		SysUserDetails sysUser = (SysUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User entity = new User();
		entity.setId(sysUser.getId());
		
		if(user.getAvatar() != null) { // 修改图像
			entity.setAvatar(user.getAvatar());
		}else if(user.getPassword() != null) { // 修改密码
			if(!user.getPassword().equals(user.getRePassword())) {
				return Result.validateFailed("重复输入的密码不一致！");
			}
			entity .setPassword(user.getPassword());
		}else {
			return Result.failed();
		}
		return Result.success(userService.update(entity));
	}
	
	@Operation(summary = "分页查询")
	@PostMapping("/page")
	public Result<IPage<User>> queryPage(@RequestBody UserQueryParam params) {
		
		return Result.success(userService.queryPage(params));
	}
}
