package com.pp.live.app.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pp.live.base.entity.User;
import com.pp.live.base.security.model.SysUserDetails;
import com.pp.live.app.service.IUserService;

/**
 * 系统用户认证
 *
 * @author haoxr
 */
@Service
public class SysUserDetailsService implements UserDetailsService {

	@Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    	User user = userService.getByUsername(username);
        
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        
        SysUserDetails userDetails = new SysUserDetails();
        userDetails.setId(user.getId());
        userDetails.setUsername(username);
        userDetails.setPassword(user.getPassword());
        userDetails.setAvatar(user.getAvatar());
        userDetails.setCoin(user.getCoin());
        userDetails.setEnabled(true);
        
        return userDetails;
    }
}
