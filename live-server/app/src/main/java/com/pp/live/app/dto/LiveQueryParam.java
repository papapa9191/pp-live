package com.pp.live.app.dto;

import com.pp.live.base.entity.Live;
import com.pp.live.base.model.PageQuery;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class LiveQueryParam extends PageQuery<Live>{
	
	@Schema(description = "用户ID")
    private Integer userId;

	@Schema(description = "直播标题")
    private String title;
}
