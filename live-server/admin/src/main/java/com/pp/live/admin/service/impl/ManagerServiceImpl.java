package com.pp.live.admin.service.impl;

import com.pp.live.admin.service.IManagerService;
import com.pp.live.base.entity.Manager;
import com.pp.live.base.mapper.ManagerMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, Manager> implements IManagerService {

}
