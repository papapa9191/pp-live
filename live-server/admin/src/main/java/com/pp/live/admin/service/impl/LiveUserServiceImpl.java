package com.pp.live.admin.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pp.live.admin.service.ILiveUserService;
import com.pp.live.base.entity.LiveUser;
import com.pp.live.base.mapper.LiveUserMapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class LiveUserServiceImpl extends ServiceImpl<LiveUserMapper, LiveUser> implements ILiveUserService {

}
