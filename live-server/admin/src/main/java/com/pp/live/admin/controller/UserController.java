package com.pp.live.admin.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Tag(name = "用户中心")
@RestController
@RequestMapping("/user")
public class UserController {
	
}
