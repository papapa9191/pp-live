package com.pp.live.admin.service;

import com.pp.live.base.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
public interface IOrderService extends IService<Order> {

}
