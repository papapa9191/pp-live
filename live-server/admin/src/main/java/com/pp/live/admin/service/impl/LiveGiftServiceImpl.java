package com.pp.live.admin.service.impl;

import com.pp.live.admin.service.ILiveGiftService;
import com.pp.live.base.entity.LiveGift;
import com.pp.live.base.mapper.LiveGiftMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
@Service
public class LiveGiftServiceImpl extends ServiceImpl<LiveGiftMapper, LiveGift> implements ILiveGiftService {

}
