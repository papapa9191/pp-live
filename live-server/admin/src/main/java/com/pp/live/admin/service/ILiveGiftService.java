package com.pp.live.admin.service;

import com.pp.live.base.entity.LiveGift;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
public interface ILiveGiftService extends IService<LiveGift> {

}
