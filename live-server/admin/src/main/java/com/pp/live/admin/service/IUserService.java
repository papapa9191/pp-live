package com.pp.live.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pp.live.base.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author baomidou
 * @since 2024-03-07
 */
public interface IUserService extends IService<User> {


}
