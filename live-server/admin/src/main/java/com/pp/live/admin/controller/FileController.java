package com.pp.live.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pp.live.base.model.Result;
import com.pp.live.base.oss.service.IOssService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;

/**
 * MinIO对象存储管理
 * Created by macro on 2019/12/25.
 */
@ConditionalOnBean(IOssService.class)
@Tag(name = "MinIO对象存储管理")
@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {
	
	@Autowired
	private IOssService ossService;

    @Operation(summary = "文件上传")
    @PostMapping(value = "/upload")
    public Result<?> upload(@RequestPart("file") MultipartFile file) {
        try {
        	String path = ossService.uploadFile(file);
        	log.info("文件上传成功:{}", path);
            return Result.success(path);
        } catch (Exception e) {
        	log.error("Exception:{}", e.getMessage());
            return Result.failed(e.getMessage());
        } 
    }

    @Operation(summary = "文件删除")
    @PostMapping(value = "/delete")
    public Result<?> delete(@RequestParam("filePath") String filePath) {
        try {
            return Result.success(ossService.deleteFile(filePath));
        } catch (Exception e) {
        	log.error("Exception:{}", e.getMessage());
        	return Result.failed(e.getMessage());
        }
    }
}
