/*
Navicat MySQL Data Transfer

Source Server         : 192.168.100.233
Source Server Version : 50730
Source Host           : 192.168.100.233:3306
Source Database       : egg-live

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2024-03-15 22:56:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8mb4 NOT NULL COMMENT '评论内容',
  `live_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播间id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `live_id` (`live_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`live_id`) REFERENCES `live` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for gift
-- ----------------------------
DROP TABLE IF EXISTS `gift`;
CREATE TABLE `gift` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '礼物名称',
  `image` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '礼物图标',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '金币',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gift
-- ----------------------------
INSERT INTO `gift` VALUES ('1', '鸡蛋', '/static/gift/1.png', '10', '2023-07-23 01:31:37', '2023-07-23 01:34:09');
INSERT INTO `gift` VALUES ('2', '666', '/static/gift/2.png', '10', '2024-03-13 21:28:49', null);
INSERT INTO `gift` VALUES ('3', '烟花', '/static/gift/3.png', '10', '2024-03-13 21:28:54', null);
INSERT INTO `gift` VALUES ('4', '瞎子', '/static/gift/4.png', '20', '2024-03-13 21:29:00', null);
INSERT INTO `gift` VALUES ('5', '球球', '/static/gift/5.png', '10', '2024-03-13 21:29:04', null);
INSERT INTO `gift` VALUES ('6', '亲亲', '/static/gift/6.png', '10', '2024-03-13 21:29:08', null);
INSERT INTO `gift` VALUES ('7', '爱心', '/static/gift/7.png', '10', '2024-03-13 21:29:12', null);
INSERT INTO `gift` VALUES ('8', '花花', '/static/gift/8.png', '10', '2024-03-13 21:29:15', null);

-- ----------------------------
-- Table structure for live
-- ----------------------------
DROP TABLE IF EXISTS `live`;
CREATE TABLE `live` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '直播间标题',
  `cover` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '直播间封面',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `look_count` int(11) NOT NULL DEFAULT '0' COMMENT '总观看人数',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '总金币',
  `key` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '唯一标识',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '直播间状态 0未开播 1直播中 2暂停直播 3直播结束',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `live_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of live
-- ----------------------------

-- ----------------------------
-- Table structure for live_gift
-- ----------------------------
DROP TABLE IF EXISTS `live_gift`;
CREATE TABLE `live_gift` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `live_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播间id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `gift_id` int(11) NOT NULL DEFAULT '0' COMMENT '礼物id',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `live_id` (`live_id`),
  KEY `user_id` (`user_id`),
  KEY `gift_id` (`gift_id`),
  CONSTRAINT `live_gift_ibfk_1` FOREIGN KEY (`live_id`) REFERENCES `live` (`id`) ON DELETE CASCADE,
  CONSTRAINT `live_gift_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `live_gift_ibfk_3` FOREIGN KEY (`gift_id`) REFERENCES `gift` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of live_gift
-- ----------------------------

-- ----------------------------
-- Table structure for live_user
-- ----------------------------
DROP TABLE IF EXISTS `live_user`;
CREATE TABLE `live_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `live_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播间id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `status` int(1) DEFAULT NULL COMMENT '0->离线，1->在线',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `live_id` (`live_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `live_user_ibfk_1` FOREIGN KEY (`live_id`) REFERENCES `live` (`id`) ON DELETE CASCADE,
  CONSTRAINT `live_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of live_user
-- ----------------------------

-- ----------------------------
-- Table structure for manager
-- ----------------------------
DROP TABLE IF EXISTS `manager`;
CREATE TABLE `manager` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '管理员账户',
  `password` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '密码',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manager
-- ----------------------------
INSERT INTO `manager` VALUES ('1', 'admin', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2023-07-21 13:41:01', '2023-07-21 13:41:01');
INSERT INTO `manager` VALUES ('2', 'test1', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2023-07-21 13:55:05', '2023-07-21 14:22:39');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(100) NOT NULL DEFAULT '' COMMENT '订单号',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `status` enum('pending','success','fail') NOT NULL DEFAULT 'pending' COMMENT '支付状态',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no` (`no`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '头像',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '金币',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------

SET FOREIGN_KEY_CHECKS=1;
